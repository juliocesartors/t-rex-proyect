<?php
//SE VERIFICA EL ROL Y LA SESION DEL USUARIO SI NO CORRESPONDE ENVIARÁ A PAGINA 404 
session_start();

if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {

header ("Location: index.php");

}


?>

<!doctype html>

<?php
// SE LLAMA AL MENU DE VISITADORES
require "base_visitador.php";

?>

<html class="no-js" lang="">

  <head>

<link rel="stylesheet" href="css/scroll.css"> 

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

  <!-- scroll con transicion --> 

<script type="text/javascript"> 

$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 225) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});

</script>

<!-- scroll con transicion --> 

</head>

    <body>
<!-- INICIO DEL SISTEMA DE USUARIO VISITADORES-->

        <div id="contenedor2">
            
          <br><strong>Sistema Automatizado de Gestión y Administración de Alcancías y la Familia (SAGA-AIF)  se define como un sistema automatizado de gestión de alcancías para la Campaña Arquidiocesana en la cual se pretende ayudar  a la recaudación de fondos para el mantenimiento y cuidado de la iglesia Católica panameña, dada las necesidades de esta última en atender a la población en materia espiritual, educativa, formación, promoción y defensa de la fe. 
          <br>
          <br>

         El proyecto SAGA tiene como finalidad proporcionar un entorno web donde los usuarios (responsables de la Campaña Arquidiocesana en los distintos niveles) puedan visualizar el avance de las alcancías distribuidas y recolectadas en la fase de solidaridad de dicha campaña. Así mismo poder brindar una visión general de la distribución de las alcancías, así como la posibilidad de poder recolectar información de las familias y negocios donde se distribuye las alcancías y tomar decisiones en cuanto programas de prevención, misiones, defensa y promoción de la fe, catequesis, reuniones y/o creación de nuevos grupos pastorales, etc. Con los cuales la Iglesia Católica en la Ciudad de Panamá pueda acercarse más a los habitantes y visitantes conociendo su realidad cristiana y humana. 
         <br>
         <br>

         Todo este esfuerzo se lleva acabo gracias a la colaboración en conjunto e interdisciplinaria de la Arquidiocesis de Panamá y la Universidad Tecnológica de Panamá, a través de la Oficina de la Campaña de Arquidiocesana y estudiantes graduandos de la Facultad de Ingeniería de Sistemas Computacionales de la carrera de Licenciatura en Desarrollo de Software.
         <br><br><br><br> </strong>
         <br/> <a href="#"><img class="scrollup" src="img/arrowtop.png"/></a>
         

<!--<div class="centrarspan"> 

<img src="img/utp.jpg" target="_blank"/>  

<img src="img/40anos.png"  target="_blank"/>

</div>-->

        </div>


    </body>

</html>

<?php

require "footer.html";

?>