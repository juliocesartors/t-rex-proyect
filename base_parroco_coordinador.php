<!doctype html>
<html class="no-js" lang="">

    <head>
       <!-- SE LLAMA A TODOS LOS CSS PARA DAR FORMA A LA PAGINA-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Sistema Automatizado de Gestión de Alcancías</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/principal.css">
        <!-- <link rel="stylesheet" href="css/iconos.css"> -->

               <link rel="stylesheet" type="text/css" href="css/responsiveform.css">
        <link rel="stylesheet" media="screen and (max-width: 1200px) and (min-width: 601px)" href="css/responsiveform1.css" />
        <link rel="stylesheet" media="screen and (max-width: 600px) and (min-width: 351px)" href="css/responsiveform2.css" />
        <link rel="stylesheet" media="screen and (max-width: 350px)" href="responsiveform3.css" />

        
        <script src="js/vendor/modernizr-2.8.3.min.js"> </script>
        
        <style type="text/css">

        #contenedor1{

            /*background:url("img/sky.jpg");*/
            background:url("img/modelo5.png");
            background-size:100% 100%;
        }

        </style>

    </head>

    <body>


            
       <!-- SE LLAMA DA MENSAJE DE BIENVENIDA A LADO SUPERIOR DERECHO CON BOTON DE CIERRE DE SESION-->
         <div id="contenedor1">

            <div id="bienvenido"> <?php echo 'Bienvenido(a), '.$_SESSION['login'] . ' - '; ?> <span id="logout"> <a href="logout.php"> Cerrar Sesión </a>  </span> </div>

            <a href="http://www.iglesiaerestu.org/" target="_blank"> <img id="logo" src="img/logo.png"/>  </a>


        <div id="menu">

             <a href="inicio.php"> <div class="boton"> Inicio  </div> </a>

       <!-- MENU PRINCIPAL DE LOS ROLES PARROCO Y COORDINADOR PARROQUIAL-->
                <select name='takeation' class="boton" onchange="window.open(this.options[this.selectedIndex].value,'_top')">

                    <div class="boton"> <option class="head">Servicios</option> </div>

                    <div class="boton"> 


                        <optgroup label="Captar Datos">

                            <option value='capturar.php'>Con Mapa</option>
                            <option value='captar_manual.php'> Manual</option>
                            <option value='devolver_alcancias.php'> Devolver Alcancías</option>

                        </optgroup>


                     </div>

                     <div class="boton"> 


                        <optgroup label="Registrar">

                            <option value='registrarse.php'>Registrar Usuarios</option>
                          <!--  <option value='registro_alcancia_visitador.php'>Registrar Alcancias por visitador</option> 
                             <option value='modificar.php'>Modificar Alcancía Repartida</option>-->

                        </optgroup>


                     </div> 

                     <div class="boton"> 


                        <optgroup label="Reporte de Alcancías por Vicarías">

                            <option value='tablaespecifica2.php'>Reporte</option>

                        </optgroup>


                     </div>

                     <div class="boton"> 


                        <optgroup label="Consulta de alcancías por vicaría">

                            <option value='alcancias_registradas2.php'>Consultar</option>
                            <option value='informecoordinador.php'>Ver estado de las alcancías</option>
                            
                        </optgroup>


                     </div>

                    
                </select>


                <select name='takeation' class="boton" onchange="window.open(this.options[this.selectedIndex].value,'_top')">

                    <div class="boton"> <option class="head">Sobre Nosotros</option> </div>
                    <div class="boton"> <option value='mision.php'>Misión</option> </div>
                    <div class="boton"> <option value='vision.php'>Visión</option> </div>
                    
                </select>
             <a href="ayuda.php"> <div class="boton"> Ayuda  </div> </a>


            </div>
        </div>

    </body>

</html>
