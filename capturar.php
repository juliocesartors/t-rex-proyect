<?php

session_start();

if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {

header ("Location: index.php");

}


?>
<!--ESTE ARCHIVO CAPTA LAS ALCANCIAS QUE SE VAN REPARTIENDO A LAS DIFERENTES FAMILIAS DE FORMA GEOLOCALIZADA , CON EL MAPA INCLUIDO
 1.VERIFICAMOS LA SESION DEL USUARIO 
 -->

<!doctype html>
<!--DEPENDIENDO DE LOS ROLES SE MOSTRARÁ UNA BASE QUE SERÁ EL MENU AL CUAL TENDRÁN ACCESO LOS DIFERENTES TIPOS DE USUARIOS.  
 -->
<?php

$rol = $_SESSION['rol'];

                  if($rol =="Visitador"){

                    require "base_visitador.php";
                  }

                  elseif($rol=="Coordinador Parroquial"){
                 require "base_parroco_coordinador.php";

                   
                  }

                  elseif($rol=="Oficina"){
                    require "base.php";
                  }

                  elseif($rol=="Sacerdote"){
                    header('base_parroco_coordinador.php');
                  }

                  elseif($rol=="Coordinador Sectorial"){
                    header('base_parroco_coordinador.php');
                  }
               

                   elseif($rol=="Coordinador Vicarial"){
                    header('base_parroco_coordinador.php');
                  }

?>

<html class="no-js" lang="">

    <head>
        
        <link rel="stylesheet" href="css/registro.css">
         <link rel="stylesheet" href="css/reportes.css"> 
         <link rel="stylesheet" href="css/scroll.css"> 

        <!--TODAS LAS FUNCIONES DEL MAPA EMPIEZAN AQUÍ UTILIZANDO JAVASCRIPT  -->

        <script src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<style> #map { width: 70%; height: 300px; border: 1px solid black; margin-left:15%;} </style> 

<script> 
function localize() { 
if (navigator.geolocation) { 
navigator.geolocation.getCurrentPosition(mapa,error); 
} else { 
alert('Tu navegador no soporta geolocalizacion.'); 
} 
} 
function mapa(pos) { /************************ variables de latitud y longitud las declaramos y les damos valores a través de un getElement***********************************/ 
var latitud = pos.coords.latitude; 
var longitud = pos.coords.longitude; 
//var precision = pos.coords.accuracy; 
var contenedor = document.getElementById("map") 
document.getElementById("lti").innerHTML=latitud;
document.getElementById("lgi").innerHTML=longitud;  
//document.getElementById("psc").innerHTML=precision; 
var centro = new google.maps.LatLng(latitud,longitud); 
var propiedades = { zoom: 15, center: centro, mapTypeId: google.maps.MapTypeId.ROADMAP }; 
var map = new google.maps.Map(contenedor, propiedades); 
var marcador = new google.maps.Marker({ position: centro, map: map, title: "Tu posicion actual" }); 
} 

// funcion que verifica si el usuario dio permiso de localizacion en el dispositivo que utiliza y alertando del error ya sea por falta de conexcion o error de usuario
function error(errorCode) { 
if(errorCode.code == 1) 
alert("No has permitido buscar tu localizacion") 
else if (errorCode.code==2) 
alert("Posicion no disponible") 
else 
alert("Ha ocurrido un error") 
} 

</script> 

        <!--TERMINA FUNCIONES DE MAPA  -->

        <!--La insercion de datos del mapa empieza aqui -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>
      var x = document.getElementById("demo");
      function getLocation()
      {
        if (navigator.geolocation)
        {
          navigator.geolocation.getCurrentPosition(bindPosition);
        }
        else {
          x.innerHTML = "La Geolocalizacion no esta disponible para este navegador.";
        }
      }
      function bindPosition(position) {
        $("input[name='latitude']").val(position.coords.latitude);
        $("input[name='longitude']").val(position.coords.longitude);
      }
    </script>

        <!--La insercion de datos del mapa termina aqui -->

        <!--El select empieza aqui -->

      <script type="text/javascript" src="js/dynamicoptionlist.js"></script>



        
        <!--El select termina aqui -->

<!-- scroll con transicion --> 

<script type="text/javascript"> 

$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 225) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});

</script>

<!-- scroll con transicion --> 

    </head>

    <body onLoad="localize(); getLocation()">

        

        <div id="contenedor2" >
            
<!--formulario de captacion de datos-->

            <div id="contenedor3">

                <br/><center> <u><h2>Captar Datos de Alcancías</h2></u> </center>  
                 <span class="obligatorio">* Significa que el campo es obligatorio</span>
                <form method="post" action="capturar_res.php">

                <strong> Código de alcancía: </strong> <br> <span class="obligatorio"> * </span> <br>
                <input type="text" name="codal" placeholder="Codigo asignado a la alcancía" required>
                <br>


                <strong> Fecha de entrega: </strong> <br> <span class="obligatorio"> * </span><input type="date"name="entrega"  min="<?php echo date('Y-m-d'); ?>" value="<?php echo date('Y-m-d'); ?>" required> 
              <br>
                <br>

                <strong> Familia a la que se entrega: </strong><span class="obligatorio"> * </span> <br>
                <input type="text" name="ap" placeholder="Nombre de la familia" required>
                <br>
                 
 
  <strong> Teléfono de la familia: </strong> <br>
                <input type="text" name="tel" placeholder="Telefono de casa o celular" >
                <br>
<br>
<?php
//require "vicarias.html";
?>
  
 <strong> Observaciones: </strong> <br>
                 <textarea name="direccion" rows="5" cols="30" placeholder=" observaciones de  dirreción de la familia"></textarea required>  
                <br>

<br>
<br>

 
<!--despliegue y llamado del mapa 
 -->
<div>
    <u><h2>Geoposicionamiento de Alcancía</h2></u>
</div>

<strong><p>Latitud: <span id="lti"></span></p></strong>
<strong><p>Longitud: <span id="lgi"></span></p></strong>
<!-- <strong><p>Presici&oacute;n: <span id="psc"></span></p> </strong> -->

<div id="mapa">

    <div id="map" ></div>

</div>

<input type='hidden' value='' name='latitude'/>
<input type='hidden' value='' name='longitude'/>
<input type='hidden' value="<?php echo $_SESSION['login']; ?>" name='lo'/>
<input type='hidden' value="<?php echo $_SESSION['ced']; ?>" name='ced'/>

<input type='hidden' value="<?php echo $_SESSION['parroquia']; ?>" name='parroquia'/>
<input type='hidden' value="<?php echo $_SESSION['vicaria']; ?>" name='vicaria'/>

<br>



 
<br>
 <!--botones de aceptar o cancelar  -->
<div class="imagenes">

<button class= "icon" name="submit" type="submit" style='width:70px; height:50px'  ><img src="css/fondos/checkmark.png"></button>
<button class= "icon" name="reset" type="reset" style='width:70px; height:50px'  ><img src="css/fondos/cross.png"></button>
<br/> <a href="#"><img class="scrollup" src="img/arrowtop.png"/></a>


</div>


<br>
<br>


            </form>
            </div>



          
       
        </div>


    </body>

</html>
<!--llamado del footer que dará forma al pie de pagina de la pagina. 
 -->
<?php

require "footer.html";

?>