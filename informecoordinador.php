<?php
session_start();
if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {

header ("Location: index.php");

}

if ($_SESSION['rol'] =='visitador' || $_SESSION['rol'] == 'Oficina') {

header ("Location: 404.html");

}


?>

<!doctype html>
    <!-- LLAMA A LA BASE DE CSS PARA DAR ESTILO -->
<?php

require "base_parroco_coordinador.php";

?>


<html>
<head>
<style>

table, th, td {
     border: 1px solid black;
}
th{
  background: gray;
/*position:fixed; */
}

table.scroll {
    /*width: 100%; 
     border-collapse: collapse; */
    border-spacing: 0;
    /*border: 2px solid black;*/
}

table.scroll tbody,
table.scroll thead { display: inline-block;}

thead tr th { 
    height: 30px;
    line-height: 30px;
    /*text-align: left;*/
}

table.scroll tbody {
    height: 250px;

    overflow-y: auto;
    overflow-x: hidden;
}

tbody { /*border-top: 2px solid black;*/ }

tbody td, thead th {
    width: 20%; /* Optional */
    /*border-right: 1px solid black;*/
}

tbody td:last-child, thead th:last-child {
    border-right: none;
}

</style>

</head>

<body>
 <center>

        <div id="contenedor2">

          <div id="contenedor3">
 
 <div id="tabla">
 <?php

        /*

          se muestra informacion del status actual de las alcancías, los cuales son:
          alcancia disponible, alcancia devuelta, y 'hoy es el dia de entrega'

        */
$conexion = new mysqli("mysql.campanaarquidiocesana.com","mgcdb","proyectosaga","campana_saga");

              if($conexion -> connect_error){
                
                die("Error en la conexion!");
                
              }

              else {
                
                
                echo("<u><h2>Informe sobre el estatus de las alcancías </h2></u><br/>");
                
              }

$vicaria = $_SESSION['vicaria'];
$parroquia = $_SESSION['parroquia'];
$sql = "SELECT `familia`.codigo_alcancia, `alcancia`.codigo_alcancia, `alcancia`.codigo_vicaria, `alcancia`.codigo_parroquia, `alcancia`.estado 
         FROM        `familia` INNER JOIN  `alcancia` ON `familia`.codigo_alcancia = `alcancia`.codigo_alcancia WHERE `alcancia`.codigo_vicaria = '$vicaria' AND `alcancia`.codigo_parroquia = '$parroquia'";

$result = $conexion->query($sql);



if ($result->num_rows > 0) {

    echo '<table id="testTable" class="scroll"> <tr> <th>Codigo de Alcancia</th> <th>Codigo de Parroquia</th> <th>Codigo de Vicaria</th> <th>Estado de la alcancía</th> </tr>';

    while($row = $result->fetch_assoc()) {
      
        echo "<tr><td>".$row["codigo_alcancia"]."</td><td>".$row["codigo_parroquia"]."</td><td>".$row["codigo_vicaria"]."</td><td>".$row["estado"]."</td></tr>";


    }
    echo '</table>';
} else {
    echo "0 Resultados.";
}

$conexion->close();

?>
</div>
<br>
               
         <!-- <button class= "boton" type="button" onclick="tableToExcel('testTable', 'W3C Example Table')"  style='width:35%; height:10%'>Guardar en Excel <img src="css/fondos/save.png"> </button>
         <br/> <a href="#"><img class="scrollup" src="img/arrowtop.png"/></a> -->

           <br>

</div>
      </div>

</body>

</html>
<?php

require "footer.html";

?>