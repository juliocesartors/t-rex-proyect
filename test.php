
<!doctype html>
<html>
<head>

<style>

.container {
  
  width:100%;
  height: 100%;
  background-color: black;
  /*margin: 0 auto;*/
  /*text-align: center;*/
  position: relative;
}
.container div {
  background-color: white;
  width: 100%;
  display: inline-block;
  display: none;
}
.container img {
  width: 100%;
  height: 100%;

}


</style>

</head>

<body>

<section class="demo">

  <div class="container">
    <div style="display: inline-block;">
      <img src="img/slider (0).jpg"/>
    </div>
    <div>
     <img src="img/slider (1).png"/>
    </div>
    <div>
      <img src="img/slider (2).png"/>
    </div>


  </div>
</section>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js?ver=1.4.2"></script>
<script type="text/javascript">

var currentIndex = 0,
  items = $('.container div'),
  itemAmt = items.length;

function cycleItems() {
  var item = $('.container div').eq(currentIndex);
  items.hide();
  item.css('display','inline-block');
}

var autoSlide = setInterval(function() {
  currentIndex += 1;
  if (currentIndex > itemAmt - 1) {
    currentIndex = 0;
  }
  cycleItems();
}, 3000);

$('.next').click(function() {
  clearInterval(autoSlide);
  currentIndex += 1;
  if (currentIndex > itemAmt - 1) {
    currentIndex = 0;
  }
  cycleItems();
});

$('.prev').click(function() {
  clearInterval(autoSlide);
  currentIndex -= 1;
  if (currentIndex < 0) {
    currentIndex = itemAmt - 1;
  }
  cycleItems();
});
</script>

</body>

</html>