<!DOCTYPE html>
<html>
<head>
<style>

table, th, td {
     border: 1px solid black;
}

table.scroll {
    width: 100%; 
    /* border-collapse: collapse; */
    border-spacing: 0;
    border: 2px solid black;
}

table.scroll tbody,
table.scroll thead { display: block; }

thead tr th { 
    height: 30px;
    line-height: 30px;
    /*text-align: left;*/
}

table.scroll tbody {
    height: 250px;
    overflow-y: auto;
    overflow-x: hidden;
}

tbody { border-top: 2px solid black; }

tbody td, thead th {
    width: 20%; /* Optional */
    border-right: 1px solid black;
}

tbody td:last-child, thead th:last-child {
    border-right: none;
}

</style>

 <script>

  var tableToExcel = (function() {
          var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
          return function(table, name) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
            window.location.href = uri + base64(format(template, ctx))
          }
        })()


</script>

<script>

// Change the selector if needed
var $table = $('table.scroll'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

// Adjust the width of thead cells when window resizes
$(window).resize(function() {
    // Get the tbody columns width array
    colWidth = $bodyCells.map(function() {
        return $(this).width();
    }).get();
    
    // Set the width of thead columns
    $table.find('thead tr').children().each(function(i, v) {
        $(v).width(colWidth[i]);
    });    
}).resize(); // Trigger resize handler

</script>
</head>

<body>
<?php

session_start();

if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {

header ("Location: index.php");

}




?>

<!doctype html>

<?php

require "base.php";


if ($_SESSION['rol'] != 'Oficina' ) {

header ("Location: 404.html");

}

$conexion = new mysqli("localhost","tesis","utp.2015","saga");

              if($conexion -> connect_error){
                
                die("Error en la conexion!");
                
              }

              else {
                
                
                echo("<u><h2>Reportes general de alcancias por parroquias</h2></u><br/>");
                
              }

  $rol = $_POST['slct1'];
  $rol2 = $_POST['slct2'];
             
              $indicador = 0;

             
 $result = $conexion->query("SELECT codigo_vicaria from alcancia WHERE codigo_vicaria = '{$rol}' LIMIT 1");
$result2 = $conexion->query("SELECT codigo_parroquia from alcancia WHERE codigo_parroquia = '{$rol2}' LIMIT 1");


  if ($result->num_rows == 1) {

    $indicador = 1;
    

  }   

 
  
 
  if ($indicador == 1){

    echo "Alcancias repartidas en la vicaria: ".$rol."<n/> <n/>y parroquia: <n/> <n/>".$rol2."<br/>";

  }




$sql = "SELECT `codigo_alcancia`, `codigo_parroquia`, `codigo_vicaria`, `fecha_entrega`, `fecha_devolucion` FROM `alcancia` WHERE codigo_vicaria ='{$rol}' AND codigo_parroquia = '{$rol2}' ";

$result = $conexion->query($sql);

if ($result->num_rows > 0) {

    echo '<div id="div1"><table id="testTable" class="scroll"> <tr> <th>Codigo de Alcancia</th> <th>Codigo de Parroquia</th> <th>Codigo de Vicaria</th>  <th>Fecha de entrega</th> <th>Fecha de devolucion</th> </tr>';

    while($row = $result->fetch_assoc()) {

        echo "<tr><td>".$row["codigo_alcancia"]."</td><td>".$row["codigo_parroquia"]."</td><td>".$row["codigo_vicaria"]."</td><td>".$row["fecha_entrega"]."</td><td>".$row["fecha_devolucion"]."</td></tr>";

    }
    echo '</table></div>';
} else {
    echo "0 Resultados.";
}

$conexion->close();

?>
<br>
               
         <button class= "boton" type="button" onclick="tableToExcel('testTable', 'W3C Example Table')"  style='width:35%; height:10%'>Guardar en Excel <img src="css/fondos/save.png"> </button>
         <br/> <a href="#"><img class="scrollup" src="img/arrowtop.png"/></a>

           <br>


</body>

</html>