<!doctype html>

<html class="no-js" lang="">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Sistema Automatizado de Gestión de Alcancías</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/principal.css">
        <!-- <link rel="stylesheet" href="css/iconos.css"> -->

        <link rel="stylesheet" type="text/css" href="css/responsiveform.css">
        <link rel="stylesheet" media="screen and (max-width: 1200px) and (min-width: 601px)" href="css/responsiveform1.css" />
        <link rel="stylesheet" media="screen and (max-width: 600px) and (min-width: 351px)" href="css/responsiveform2.css" />
        <link rel="stylesheet" media="screen and (max-width: 350px)" href="responsiveform3.css" />

       <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
        <style type="text/css">

        #contenedor1{

            /*background:url("img/sky.jpg");*/
            background:url("img/modelo5.png");
            background-size:100% 100%;
        }

        </style>

    </head>

    <body>


            

         <div id="contenedor1">

           
            <a href="http://www.iglesiaerestu.org/" target="_blank"> <img id="logo" src="img/logo.png"/>  </a>


  </div>
            


     
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="inicio.php">SAGA</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="inicio.php">Inicio</a></li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" >Servicios <span class="caret"></span></a>
        <ul class="dropdown-menu">
          
           <li class="dropdown-header">CAPTAR DATOS</li>
          <li><a href="capturar2.php">Con Mapa</a></li>
          <li><a href="captar_manual_oficina.php">Manual</a></li>
          <li><a href="devolucion_alcancias_oficina.php">Devolver Alcancía</a></li>
          <li class="divider"></li>
          <li class="dropdown-header">REGISTRAR</li>
          <li><a href="registrar_oficina.php">Registrar Usuario</a></li>
           <li class="divider"></li>
          <li class="dropdown-header">REPORTES DE ALCANCÍAS</li>
          <li><a href="reportes.php">Reportes por parroquia</a></li>
           <li><a href="tablageneral.php">Reportes General</a></li>
       <li class="divider"></li>
                 <li class="dropdown-header">CONSULTAS</li>
                    <li><a href="consultar.php">Consultas alcancías por vicarías</a></li>
           <li><a href="informe.php">Ver Estado de Vicarías</a></li>
       <li class="divider"></li>


        </ul>
      </li>
      <li><a href="mision.php">Misión</a></li>
      <li><a href="vision.php">Visión</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="index.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar Sesión</a></li>
    </ul>
  </div>
</nav>