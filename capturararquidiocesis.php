<?php

session_start();

if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {

header ("Location: index.php");

}


?>

<!doctype html>

<?php

require "base.php";

?>

<html class="no-js" lang="">

    <head>
        
        <link rel="stylesheet" href="css/registro.css">
         <link rel="stylesheet" href="css/reportes.css"> 
         <link rel="stylesheet" href="css/scroll.css"> 

     
   

        <!--El select empieza aqui -->

        <script type="text/javascript" src="js/dynamicoptionlist.js"></script>


<script type="text/javascript">

var vic = new DynamicOptionList(); 
vic.addDependentFields("vicaria","parroquia"); 
 
vic.forValue("VICARÍA DE SAN CRISTÓBAL").addOptions("San Cristóbal","Inmaculada Concepción","Jesús Buen Pastor"," Cuerpo y Sangre de Cristo","San Pedro"); 
vic.forValue("VICARÍA CRISTO REDENTOR").addOptions("Cristo Redentor","Cristo Hijo del Hombre","Cristo Servidor","El Señor de los Milagros","Cristo Hijo de Dios"); 
 vic.forValue("VICARÍA LA SANTA CRUZ").addOptions("San Martín de Porres","Ntra. Sra. del Rosario","La Sagrada Familia","San Agustín ","San Juan María Vianney"); 
 vic.forValue("VICARÍA NSTRA. SRA.DEL CARMEN").addOptions("San Judas Tadeo","San Pedro Apóstol","Ntra. Sra. de la Candelaria","Ntra. Sra.del Carmen","La Inmaculada Concepción"); 
vic.forValue("VICARÍA DE DON BOSCO").addOptions("San Antonio de Padua","Espíritu Santo","Nuestra Señora de Belén","La Ascensión del Señor ","San Juan Bosco"); 
vic.forValue("VICARIA LA MERCED").addOptions("Santa Teresita","San Miguel Arcángel","Basílica Menor Don Bosco","San Vicente de Paúl ","Santa Ana");
vic.forValue("VICARIA CRISTO REY").addOptions("San Francisco de la Caleta", "Santuario Nacional","San Mateo","Nuestra Señora del Carmen","Cristo Rey");
vic.forValue("VICARIA SANTA EDUVIGIS").addOptions("Santa Marta","Santísima Trinidad","Nuestra Señora de los Angeles","San Pablo Apóstol","Santa Eduvigis");
 vic.forValue("VICARÍA DE LA ASUNCIÓN").addOptions("San Gerardo Mayela","Nuestra Señora de Lourdes"," María Auxiliadora"," San Antonio Ma. Claret ","Santiago Apóstol");
vic.selectFirstOption = false;

</script>
        
        <!--El select termina aqui -->

<!-- scroll con transicion --> 

<script type="text/javascript"> 

$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 225) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});

</script>

<!-- scroll con transicion --> 

    </head>

    <body >

        

        <div id="contenedor2" >
            


            <div id="contenedor3">

                <br/><center> <u><h2>Formulario de Entrega de Alcancias para la arquidiocesis</h2></u> </center>  

                <form method="post" action="">

                <strong> Código de alcancía: </strong> <br>
                <input type="text" name="codal" placeholder="Codigo asignado a la alcancía" >
                <br>


                <strong> Fecha de entrega: </strong> <br>
                <input type="text" name="entrega" placeholder="Fecha de entrega de la alcancía" >
                <br>

              
 
  <strong> Teléfono Parroquia: </strong> <br>
                <input type="text" name="tel" placeholder="Telefono de casa o celular" >
                <br>
<br>



<strong> Vicaría a la que pertenece a familia: </strong> <br>
 
<select name="vicaria" class="select"> 

<option selected="selected" value="VICARÍA DE SAN CRISTÓBAL">VICARÍA DE SAN CRISTÓBAL</option> 

<option value="VICARÍA CRISTO REDENTOR">VICARÍA CRISTO REDENTOR</option> 
<option value="VICARÍA LA SANTA CRUZ">VICARÍA LA SANTA CRUZ</option> 
<option value="VICARÍA NSTRA. SRA.DEL CARMEN">VICARÍA NSTRA. SRA.DEL CARMEN</option> 
<option value="VICARÍA DE DON BOSCO">VICARÍA DE DON BOSCO</option> 
<option value="VICARIA LA MERCED">VICARIA LA MERCED</option> 
<option value="VICARIA CRISTO REY">VICARIA CRISTO REY</option> 
<option value="VICARIA SANTA EDUVIGIS">VICARIA SANTA EDUVIGIS</option> 
<option value="VICARÍA DE LA ASUNCIÓN">VICARÍA DE LA ASUNCIÓN</option> 

</select> 


<strong> Parroquia a la que pertenece la familia: </strong> <br>

<select name="parroquia" class="select"> 

<script type="text/javascript">vic.printOptions("parroquia")</script> 

</select> 



<br>

 
 <br>
 
<br>
 
<div class="imagenes">

<button class= "icon" name="submit" type="submit" style='width:70px; height:50px'  ><img src="css/fondos/checkmark.png"></button>
<button class= "icon" name="reset" type="reset" style='width:70px; height:50px'  ><img src="css/fondos/cross.png"></button>
<br/> <a href="#"><img class="scrollup" src="img/arrowtop.png"/></a>

</div>


<br>
<br>


            </form>
            </div>



          
       
        </div>


    </body>

</html>

<?php

require "footer.html";

?>