<?php

session_start();

if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {

header ("Location: index.php");

}




?>
<!DOCTYPE html>
<style>
table, th, td {
     border: 1px solid black;
}

table.scroll {
    width: 100%; 
    /* border-collapse: collapse; */
    border-spacing: 0;
    border: 2px solid transparent;
}

th{
  background: gray;
/*position:fixed; */
}

table.scroll tbody,
table.scroll thead {     
  display: inherit;
    margin-left: -25%; 
  }

thead tr th { 
    height: 30%;
    line-height: 30%;
    /*text-align: left;*/
}

table.scroll tbody {
    height: 100%;
    width: 100%;

}

tbody { border-top: 2px solid black; }

tbody td, thead th {
    
    border-right: 1px solid black;
}

tbody td:last-child, thead th:last-child {
    border-right: none;
}
</style>



<?php

        /*

          Se valida el rol del usuario para posteriormente redirigirlo a la pagina de inicio correspondiente


        */

$rol = $_SESSION['rol'];

                if($rol=="Coordinador Parroquial"){
                 require "base_parroco_coordinador.php";

                   
                  }

                  elseif($rol=="Oficina"){
                    require "base.php";
                  }

                  elseif($rol=="Sacerdote"){
                    header('base_parroco_coordinador.php');
                  }

                  elseif($rol=="Coordinador Sectorial"){
                    header('base_parroco_coordinador.php');
                  }
               

                   elseif($rol=="Coordinador Vicarial"){
                    header('base_parroco_coordinador.php');
                  }



?>

<html class="no-js" lang="">
<head>

<meta charset="utf-8">

 
</head>
<body>



<center>
<?php


        /*

          se efectua la conexion a la BD, validando si hubo o no un error en la conexion

        */

$conexion = new mysqli("mysql.campanaarquidiocesana.com","mgcdb","proyectosaga","campana_saga");

              if($conexion -> connect_error){
                
                die("Error en la conexion!");
                
              }

              else {
                
  ?>
<form method="post" action="#" onsubmit="resultado()">      
 <?php         

         /*

          se obtienen los datos de los campos de vicaria y parroquia en los campos llamados vicaria y parroquia, respectivamente. Al realizar esto,se realiza la consulta.
          posterior a eso se reaiza la consulta a la base de datos, asignando en opciones de select todas las parroquias y vicarias.


        */
                echo("<u><h2>Consulta de alcancias por parroquias</h2></u><br/>");
                
              }

$vicaria = $_SESSION['vicaria'];
$parroquia = $_SESSION['parroquia'];
             
              $indicador = 0;

             
$result = $conexion->query("SELECT codigo_vicaria from alcancia WHERE codigo_vicaria = '{$vicaria}' LIMIT 1");
$result2 = $conexion->query("SELECT codigo_parroquia from alcancia WHERE codigo_parroquia = '{$parroquia}' LIMIT 1");
$result3 = $conexion->query("SELECT nombre from parroquia WHERE codigo_parroquia = '{$parroquia}' ");
$result4 = $conexion->query("SELECT nombre from vicaria WHERE codigo_vicaria = '{$vicaria}' ");
  if ($result->num_rows == 1) {

    $indicador = 1;
    

  }   


 
if ($indicador == 1 && $result3->num_rows > 0 && $result4->num_rows > 0){
$row2= $result3->fetch_assoc();
$row3= $result4->fetch_assoc();

    echo "<strong>Alcancias registrada en la vicaria: <u> ".$row3['nombre']."</u><n/> <n/> y parroquia: <n/> <n/><u>".$row2['nombre']."</u></strong><br/><br>";

  }



$sql = "SELECT `codigo_alcancia` FROM `alcancia` WHERE codigo_vicaria ='{$vicaria}' AND codigo_parroquia = '{$parroquia}' ORDER BY `codigo_alcancia` ASC ";

$result = $conexion->query($sql);

if ($result->num_rows > 0  ) {
   echo "<select  id='select1' name='select1' onchange='this.form.submit()' >";

    while($row = $result->fetch_assoc() ) {
   
    echo "<option value='".$row['codigo_alcancia']."'>".$row['codigo_alcancia']."</option>";

    }

   echo "</select>";
   
} else {
    echo "0 Resultados.";
}

$conexion->close();

?>
<br>
<br>
<br>
               
         <div class="imagenes">

                <button class= "icon" name="submit" type="submit" style='width:70px; height:50px'  ><img src="css/fondos/checkmark.png"></button>
                

                </div>

</form>          



  
 <?php     

         /*

          en este bloque de codigo se muestran todas las alcancias registradas dependiendo de la parroquia y vicaria que se seleccione.
          no sin antes haber realizado la conexion a la base de datos y validar el estado de conexion de la misma.
          despues eso se realizo la consulta a las tablas alcancia y familia utilizando la tecnica SQL del "inner join"

        */
 function resultado(){ 


 $conexion = new mysqli("mysql.campanaarquidiocesana.com","mgcdb","proyectosaga","campana_saga");

              if($conexion -> connect_error){
                
                die("Error en la conexion!");
                
              }

              else {
       
                
              }

  $alcancia = $_POST['select1'];

             
       
$result3 = $conexion->query("SELECT `familia`.codigo_alcancia,`familia`.apellido,`familia`.direccion, `alcancia`.codigo_alcancia, `alcancia`.codigo_vicaria, `alcancia`.codigo_parroquia, `alcancia`.fecha_entrega,`alcancia`.fecha_devolucion
         FROM        `familia` INNER JOIN  `alcancia` ON `familia`.codigo_alcancia = `alcancia`.codigo_alcancia WHERE `alcancia`.codigo_alcancia ='{$alcancia}'" );



if ($result3->num_rows > 0 ) {
    echo "<strong>Alcancias registrada ".$alcancia."</strong><br/><br>";

    echo '<div id="div1"><table id="testTable" class="scroll" > <tr> <th>Codigo de Alcancia</th><th>Familia</th><th>Dirección</th><th>Fecha de Entrega</th><th>Fecha de Devolución</th></tr>';

    while($row2= $result3->fetch_assoc()) {
   
        echo "<tr><td>".$row2["codigo_alcancia"]."</td><td>".$row2["apellido"]."</td><td>".$row2["direccion"]."</td><td>".$row2["fecha_entrega"]."</td><td>".$row2["fecha_devolucion"]."</td></tr>";

    }

    echo '</table></div>';
   
} else {
    echo "0 Resultados.";
}


$conexion->close();
}

?>

<?php

echo resultado();

      ?>  



</body>
<center>
</html>

<?php

require "footer.html";

?>