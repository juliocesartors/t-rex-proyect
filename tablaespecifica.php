<?php

session_start();

if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {

header ("Location: index.php");

}

if ($_SESSION['rol'] =='visitador') {

header ("Location: 404.html");

}



?>
<?php

require "base.php";

?>
<!DOCTYPE html>
<html>
<head>
 <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/scroll.css"> 
    
<style>

table, th, td {
     border: 1px solid black;
}

table.scroll {
    width: 100%; 
    /* border-collapse: collapse; */
    border-spacing: 0;
    border: 2px solid black;
        margin-left: -0%;
}

table.scroll tbody,
table.scroll thead { display: block; }



table.scroll tbody {
    height: 250px;
    overflow-y: auto;
    overflow-x: hidden;
}

tbody { border-top: 2px solid black; }

tr td {
  width: 20%; /* Optional */
  border-right: 1px solid black;

}
 th {
    width: 20%; /* Optional */
    background: #707070;
    border-right: 1px solid black;
}

tbody td:last-child, thead th:last-child {
    border-right: none;
}
thead{
  background: #707070;
  text-align: center;
  display: inline-block;
   
}
</style>

 <script>

  var tableToExcel = (function() {
          var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
          return function(table, name) {
            if (!table.nodeType) table = document.getElementById(table)
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
            window.location.href = uri + base64(format(template, ctx))
          }
        })()


</script>

<script>

// Change the selector if needed
var $table = $('table.scroll'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

// Adjust the width of thead cells when window resizes
$(window).resize(function() {
    // Get the tbody columns width array
    colWidth = $bodyCells.map(function() {
        return $(this).width();
    }).get();
    
    // Set the width of thead columns
    $table.find('thead tr').children().each(function(i, v) {
        $(v).width(colWidth[i]);
    });    
}).resize(); // Trigger resize handler

</script>

<script type="text/javascript"> 

$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 225) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});

</script>
</head>

<body>
 <center>

        <div id="contenedor2">

          <div id="contenedor3">
 
 <?php


//$conexion = new mysqli("mysql.campanaarquidiocesana.com","mgcdb","proyectosaga","campana_saga");
  $conexion = new mysqli("localhost","tesis","utp.2015","saga");

              if($conexion -> connect_error){
                
                die("Error en la conexion!");
                
              }

              else {
                
                
                echo("<u><h2>Reporte general de alcancias por parroquias</h2></u><br/>");
                
              }

  $vicaria = $_POST['slct1'];
  $parroquia = $_POST['slct2'];
             
              $indicador = 0;

             
$result = $conexion->query("SELECT codigo_vicaria from alcancia WHERE codigo_vicaria = '{$vicaria}' LIMIT 1");
$result2 = $conexion->query("SELECT codigo_parroquia from alcancia WHERE codigo_parroquia = '{$parroquia}' LIMIT 1");
$iglesia = $conexion->query("SELECT nombre from parroquia WHERE codigo_parroquia = '{$parroquia}'");
$vica = $conexion->query("SELECT nombre from vicaria WHERE codigo_vicaria = '{$vicaria}'");

$result3 = $conexion->query("SELECT `familia`.codigo_alcancia,`familia`.apellido,`familia`.direccion, `alcancia`.codigo_alcancia, `alcancia`.codigo_vicaria, `alcancia`.codigo_parroquia, `alcancia`.fecha_entrega,`alcancia`.fecha_devolucion
         FROM        `familia` INNER JOIN  `alcancia` ON `familia`.codigo_alcancia = `alcancia`.codigo_alcancia where `alcancia`.codigo_vicaria = '{$vicaria}' and `alcancia`.codigo_parroquia = '{$parroquia}' " );



  if ($result->num_rows == 1) {

    $indicador = 1;

    

  }   


  if ($indicador == 1 && $iglesia->num_rows > 0){
     $row2= $iglesia->fetch_assoc();
       $rowvic= $vica->fetch_assoc();
    echo "<strong>Alcancias registrada en la vicaria: <u>".$rowvic['nombre']."</u><n/> <n/>y parroquia: <n/> <n/><u>".$row2['nombre']."</u></strong><br/><br>";

  }
  

$sql = "SELECT `codigo_alcancia` FROM `alcancia` WHERE codigo_vicaria ='{$vicaria}' AND codigo_parroquia = '{$parroquia}' ORDER BY `codigo_vicaria` ASC ";

$result = $conexion->query($sql);

$total1 = $conexion->query("SELECT COUNT(codigo_alcancia) AS cantidad_alcancia FROM alcancia WHERE codigo_vicaria ='{$vicaria}' AND codigo_parroquia = '{$parroquia}'");
$total2 = $conexion->query("SELECT COUNT(codigo_alcancia) AS cantidad_alcancia FROM alcancia WHERE estado='Entregada' AND codigo_vicaria ='{$vicaria}' AND codigo_parroquia = '{$parroquia}'");
$total3 = $conexion->query("SELECT COUNT(codigo_alcancia) AS cantidad_alcancia FROM alcancia WHERE estado='Devuelta' AND codigo_vicaria ='{$vicaria}' AND codigo_parroquia = '{$parroquia}'");

if ($result->num_rows > 0 && $result3->num_rows > 0 ) {
    echo ' <div class="table-responsive">  <table class="table" id="testTable" class="scroll"><tr> <th >Codigo de Alcancia</th><th >Familia</th><th >Dirección</th><th >Fecha de Entrega</th><th>Fecha de Devolución</th></tr>';
  
    while($row = $result->fetch_assoc() && $row2= $result3->fetch_assoc()) {
   
        echo "<tr><td >".$row2["codigo_alcancia"]."</td><td >".$row2["apellido"]."</td><td >".$row2["direccion"]."</td><td >".$row2["fecha_entrega"]."</td><td >".$row2["fecha_devolucion"]."</td></tr>";

    }

    echo '</table></div>';
   
} else {
    echo "0 Resultados.";
}


if ($total1->num_rows > 0) {

 
    while($row = $total1->fetch_assoc()) {

        echo '<br><b>Total de Alcancias= </b>'.'<b>'.$row["cantidad_alcancia"].'</b><br><br>';

    }
  
} else {
    echo "0 Resultados.";
}


if ($total2->num_rows > 0) {

 
    while($row = $total2->fetch_assoc()) {

        echo '<b>Entregadas= </b>'.'<b>'.$row["cantidad_alcancia"].'</b><br>';

    }
  
} else {
    echo "0 Resultados.";
}

if ($total3->num_rows > 0) {

 
    while($row = $total3->fetch_assoc()) {

        echo '<b>Devueltas= </b>'.'<b>'.$row["cantidad_alcancia"].'</b><br><br>';

    }
  
} else {
    echo "0 Resultados.";
}

$conexion->close();

?>
<br>
               
         <button class= "boton" type="button" onclick="tableToExcel('testTable', 'W3C Example Table')"  style='width:35%; height:10%'>Guardar en Excel <img src="css/fondos/save.png"> </button>
         <br/> <a href="#"><img class="scrollup" src="img/arrowtop.png" style="top:0;margin-left:102%"/></a>

           <br>


</div>
      </div>


</body>

</html>
<?php

require "footer.html";

?>